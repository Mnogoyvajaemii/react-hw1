import { Component } from "react";
import "./header.scss";
import Button from "../Button";
import Modal from "../Modal";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { showModal: false, modalData: null };

    this.handleShowModal = this.handleShowModal.bind(this);
    this.handleHideModal = this.handleHideModal.bind(this);
  }

  handleShowModal(modalData) {
    this.setState({
      showModal: true,
      modalData,
    });
  }

  handleHideModal() {
    this.setState({
      showModal: false,
      modalData: null,
    });
  }

  render() {
    const { showModal, modalData } = this.state;

    return (
      <>
        <header className="header-page">
          <Button
            onClick={() =>
              this.handleShowModal({
                header: "Do u want delete this file?",
                text: "Are u sure u want to delete it?",
                actions: [
                  <Button
                    key={1}
                    onClick={this.handleHideModal}
                    text={"Ok"}
                    backgroundColor={"rgba(100, 0, 0, 0.9)"}
                  />,
                  <Button
                    key={2}
                    onClick={this.handleHideModal}
                    text={"Close"}
                    backgroundColor={"rgba(100, 0, 0, 0.9)"}
                  />,
                ],
              })
            }
            text={"Open first modal"}
            backgroundColor={"rgb(0, 87, 183)"}
          />
          <Button
            onClick={() =>
              this.handleShowModal({
                header: "Do u want to save your changes?",
                text: "Your changes will be lost if you don't save them.",
                actions: [
                  <Button
                    key={1}
                    onClick={this.handleHideModal}
                    text={"Save"}
                    backgroundColor={"rgba(100, 0, 0, 0.9)"}
                  />,
                  <Button
                    key={2}
                    onClick={this.handleHideModal}
                    text={"Discard"}
                    backgroundColor={"rgba(100, 0, 0, 0.9)"}
                  />,
                  <Button
                    key={3}
                    onClick={this.handleHideModal}
                    text={"Cancel"}
                    backgroundColor={"rgba(100, 0, 0, 0.9)"}
                  />,
                ],
              })
            }
            text={"Open second modal"}
            backgroundColor={"yellow"}
          />
        </header>

        {showModal && modalData && (
          <Modal
            header={modalData.header}
            text={modalData.text}
            closeButton={true}
            onClose={this.handleHideModal}
            actions={modalData.actions}
          />
        )}
      </>
    );
  }
}

export default Header;
