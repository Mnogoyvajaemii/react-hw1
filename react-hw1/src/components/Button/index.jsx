import { Component } from "react";
import "./button.scss";

class Button extends Component {
  render() {
    return (
      <>
        <button className="btn" onClick={this.props.onClick} style={{ backgroundColor: this.props.backgroundColor }}>
          {this.props.text}
        </button>
      </>
    );
  }
}

export default Button;
