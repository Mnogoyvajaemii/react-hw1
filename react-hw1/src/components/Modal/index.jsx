import { Component } from "react";
import "./modal.scss";

class Modal extends Component {
  constructor(props) {
    super(props);

    this.handleOverlayClick = this.handleOverlayClick.bind(this);
  }

  handleOverlayClick(event) {
    if (event.target === event.currentTarget) {
      this.props.onClose();
    }
  }

  render() {
    return (
      <>
        <div className="modal-overlay" onClick={this.handleOverlayClick}></div>
        <div className="modal">
          {this.props.closeButton === true ? (
            <div className="modal-closedBtn" onClick={this.props.onClose}></div>
          ) : null}
          <h2 className="modal-title">{this.props.header}</h2>
          <p className="modal-descr">{this.props.text}</p>
          <div className="modal-buttons">{this.props.actions}</div>
        </div>
      </>
    );
  }
}

export default Modal;
